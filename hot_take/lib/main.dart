import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';


import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if(!kIsWeb){
    await Firebase.initializeApp();
  }
  else{
    await Firebase.initializeApp(
        options: const FirebaseOptions(
            apiKey: "AIzaSyCwWdkuViJQpgldBCz9UqbWm6PF8IcBRjo",
            authDomain: "hot-take-466c7.firebaseapp.com",
            projectId: "hot-take-466c7",
            storageBucket: "hot-take-466c7.appspot.com",
            messagingSenderId: "258517667045",
            appId: "1:258517667045:web:9ac7af09a2b73a9c65980c",
            measurementId: "G-VNE094BE2B"
        )
    );
  }

  Map<String,  dynamic> promptMap = {
    "prompt":{
      "promptText": "",
      "userResponses": {
        "playerName": "",
        "promptBlanks": {
          "response": "",
  },
      },

    },
  };

  DocumentSnapshot snapshot = await FirebaseFirestore.instance.collection("Prompts").doc("IJ5bTCc4OygwUlsV8m85").get();

  runApp(
    MaterialApp(
      title: 'Navigaion Demo',
      initialRoute: '/',
      routes: {
        '/': (context) => const MainMenu(),
        '/host': (context) => const HostScreen(),
        '/join': (context) => const JoinScreen(),
      },
    ),
  );
}

class MainMenu extends StatelessWidget{
  const MainMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            ElevatedButton(
              child: const Text('Host'),
              onPressed: () {
                Navigator.pushNamed(context, '/host');
              },
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(20, 10),
                  maximumSize: Size(200, 100),
                  textStyle: const TextStyle(fontSize: 50)
              ),
            ),
            ElevatedButton(
              child: const Text('Join'),
              onPressed: () {
                Navigator.pushNamed(context, '/join');
              },
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(20, 10),
                  maximumSize: Size(200, 100),
                  textStyle: const TextStyle(fontSize: 50)
              ),
            ),
          ]
        ),
      ),
    );
  }
}

class HostScreen extends StatelessWidget{
  const HostScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        // Overide the default Back button
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        foregroundColor: Colors.transparent,
        leading: ElevatedButton.icon(
          onPressed: () => Navigator.of(context).pop(),
          icon: const Icon(Icons.arrow_left_sharp),
          label: const Text('Back'),
          style: ElevatedButton.styleFrom(
              elevation: 0, primary: Colors.transparent),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.all(5)),
            const SizedBox(
              width: 150,
              child: TextField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Player Nickname',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}class JoinScreen extends StatelessWidget{
  const JoinScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.all(5)),
            const SizedBox(
              width: 150,
              child: TextField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Player Nickname',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}