import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';


import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

Map<String, Object> playerMap = {
  "players": [
    <String, Object>{
    "playerName": "player1",
      "icon" : "new icon",
      "roomCode": 123413451345

    }
  ]
};

class WaitingRoom extends StatelessWidget{
  const WaitingRoom(
      this.players,
      this.roomCode,
  {key? key}
      ) : super(key: key);

  final Object players;
  final int roomCode;

  @override
  Widget build(BuildContext context){
    return Column(
      children: [
        const Padding(padding: EdgeInsets.all(10)),
        SizedBox(
          height: 60,
          width: 300,
        )
      ],
    );
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if(!kIsWeb){
    await Firebase.initializeApp();
  }
  else{
    await Firebase.initializeApp(
        options: const FirebaseOptions(
            apiKey: "AIzaSyCwWdkuViJQpgldBCz9UqbWm6PF8IcBRjo",
            authDomain: "hot-take-466c7.firebaseapp.com",
            projectId: "hot-take-466c7",
            storageBucket: "hot-take-466c7.appspot.com",
            messagingSenderId: "258517667045",
            appId: "1:258517667045:web:9ac7af09a2b73a9c65980c",
            measurementId: "G-VNE094BE2B"
        )
    );
  }

  runApp(
    MaterialApp(
      title: 'Navigaion Demo',
      initialRoute: '/',
      routes: {
        '/': (context) => const MainMenu(),
        '/second': (context) => const HostScreen(),
      },
    ),
  );
}

class MainMenu extends StatelessWidget{
  const MainMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:  AppBar(
        title: const Text('First Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          child: const Text('Open Second Screen'),
          onPressed: () {
            Navigator.pushNamed(context, '/second');
          },
        ),
      ),
    );
  }
}

class HostScreen extends StatelessWidget{
  const HostScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:  AppBar(
        title: const Text('Second Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.all(5)),
            const SizedBox(
              width: 150,
              child: TextField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Player Nickname',
                ),
              ),
            ),
            ElevatedButton(
              child: const Text('Open waiting Screen'),
              onPressed: () {
                Navigator.pushNamed(context, '/waitingRoom');
              },
            ),
          ],
        ),
      ),
    );
  }
}